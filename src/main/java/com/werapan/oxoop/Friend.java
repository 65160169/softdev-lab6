/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;

/**
 *
 * @author Asus
 */
public class Friend implements Serializable{
     private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastID=1;
    
    public Friend(String name,int age,String tel){
        this.id = lastID++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }
    
    @Override
    public String toString(){
        return "Friend "+"ID: "+id+" name: "+name+" Age: "+age+" Tel: "+tel;
    }
    
     public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) throws Exception {
        if(age<0){
            throw new Exception();
        }
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    
    public static void setLastID(int lastID) {
        Friend.lastID =  lastID;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }
    
    public static int getLastID() {
        return lastID;
    }
    
    public static void main(String[] args) {
        try{
            Friend f1 = new Friend("wave",19,"123456789");
            Friend f2 = new Friend("wave2",60,"123456789");
            System.out.println(f1.toString());
            System.out.println(f2.toString());
            f1.setAge(-1);
        }
        catch(Exception ex){
            System.out.println("Holy Shit");
        }
    }


   
}
